﻿using EFCodeFirst.Extensions;
using EFCodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Data
{
    public partial class EFCodeFirstContext : DbContext
    {
        public virtual DbSet<Categories> Categories { get; set; }

        public virtual DbSet<Territories> Territories { get; set; }

        public virtual DbSet<Employees> Employees { get; set; }

        public virtual DbSet<EmployeeTerritories> EmployeeTerritories { get; set; }

        public virtual DbSet<OrderDetails> OrderDetails { get; set; }

        public virtual DbSet<Orders> Orders { get; set; }

        public virtual DbSet<Products> Products { get; set; }

        public virtual DbSet<Region> Regions { get; set; }

        public virtual DbSet<Shippers> Shippers { get; set; }

        public virtual DbSet<Suppliers> Suppliers { get; set; }

        public virtual DbSet<Customers> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Northwind-1;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // It is impossible to mark composite key using attribute notation, so we use fluent api in this case.
            modelBuilder.Entity<EmployeeTerritories>(entity =>
            {
                entity.HasKey(x => new { x.EmployeeId, x.TerritoryId })
                    .IsClustered(false);
            });

            modelBuilder.Entity<OrderDetails>(entity =>
            {
                entity.HasKey(x => new { x.OrderId, x.ProductId })
                    .IsClustered(false);
            });

            // Seed data
            modelBuilder.Seed();
        }
    }
}
