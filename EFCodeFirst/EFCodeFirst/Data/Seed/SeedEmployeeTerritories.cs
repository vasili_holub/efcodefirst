﻿using EFCodeFirst.Models;

namespace EFCodeFirst.Data.Seed
{
    public class SeedEmployeeTerritories
    {
        public EmployeeTerritories[] EmployeeTerritories =>
            new EmployeeTerritories[]
            {
                new EmployeeTerritories(){ EmployeeId = 3, TerritoryId = "30346"},
                new EmployeeTerritories(){ EmployeeId = 3, TerritoryId = "31406"},
                new EmployeeTerritories(){ EmployeeId = 3, TerritoryId = "32859"},
                new EmployeeTerritories(){ EmployeeId = 3, TerritoryId = "33607"},
                new EmployeeTerritories(){ EmployeeId = 4, TerritoryId = "20852"},
                new EmployeeTerritories(){ EmployeeId = 4, TerritoryId = "27403"},
                new EmployeeTerritories(){ EmployeeId = 4, TerritoryId = "27511"},
                new EmployeeTerritories(){ EmployeeId = 5, TerritoryId = "02903"},
                new EmployeeTerritories(){ EmployeeId = 5, TerritoryId = "07960"},
                new EmployeeTerritories(){ EmployeeId = 5, TerritoryId = "08837"},
                new EmployeeTerritories(){ EmployeeId = 5, TerritoryId = "10019"},
                new EmployeeTerritories(){ EmployeeId = 5, TerritoryId = "10038"},
                new EmployeeTerritories(){ EmployeeId = 5, TerritoryId = "11747"},
                new EmployeeTerritories(){ EmployeeId = 5, TerritoryId = "14450"},
                new EmployeeTerritories(){ EmployeeId = 6, TerritoryId = "85014"},
                new EmployeeTerritories(){ EmployeeId = 6, TerritoryId = "85251"},
                new EmployeeTerritories(){ EmployeeId = 6, TerritoryId = "98004"},
                new EmployeeTerritories(){ EmployeeId = 6, TerritoryId = "98052"},
                new EmployeeTerritories(){ EmployeeId = 6, TerritoryId = "98104"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "60179"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "60601"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "80202"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "80909"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "90405"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "94025"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "94105"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "95008"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "95054"},
                new EmployeeTerritories(){ EmployeeId = 7, TerritoryId = "95060"},
                new EmployeeTerritories(){ EmployeeId = 8, TerritoryId = "19428"},
                new EmployeeTerritories(){ EmployeeId = 8, TerritoryId = "44122"},
                new EmployeeTerritories(){ EmployeeId = 8, TerritoryId = "45839"},
                new EmployeeTerritories(){ EmployeeId = 8, TerritoryId = "53404"},
                new EmployeeTerritories(){ EmployeeId = 9, TerritoryId = "03049"},
                new EmployeeTerritories(){ EmployeeId = 9, TerritoryId = "03801"},
                new EmployeeTerritories(){ EmployeeId = 9, TerritoryId = "48075"},
                new EmployeeTerritories(){ EmployeeId = 9, TerritoryId = "48084"},
                new EmployeeTerritories(){ EmployeeId = 9, TerritoryId = "48304"},
                new EmployeeTerritories(){ EmployeeId = 9, TerritoryId = "55113"},
                new EmployeeTerritories(){ EmployeeId = 9, TerritoryId = "55439"}
            };
    }
}