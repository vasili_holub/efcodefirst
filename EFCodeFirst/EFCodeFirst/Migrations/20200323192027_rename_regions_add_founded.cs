﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirst.Migrations
{
    public partial class rename_regions_add_founded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Founded",
                table: "Customers",
                nullable: true);
            migrationBuilder.RenameTable("Regions", null, "Region", null);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Founded",
                table: "Customers");
            migrationBuilder.RenameTable("Region", null, "Regions", null);
        }
    }
}
