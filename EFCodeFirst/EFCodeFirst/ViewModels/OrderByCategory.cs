﻿namespace EFCodeFirst.ViewModels
{
    public class OrderByCategory
    {
        public string CategoryName { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int OrderId { get; set; }

        public  string CompanyName { get; set; }

        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }

        public float Discount { get; set; }
    }
}
