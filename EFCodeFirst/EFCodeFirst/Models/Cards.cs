﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EFCodeFirst.Models
{
    public class Cards
    {
        [Key]
        public int CardId { get; set; }

        public string CardNumber { get; set; }

        public DateTime? Expired { get; set; }

        public string CardHolder { get; set; }

        public int EmployeeId { get; set; }
    }
}
