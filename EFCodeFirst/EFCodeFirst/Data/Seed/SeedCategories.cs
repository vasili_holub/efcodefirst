﻿using EFCodeFirst.Models;

namespace EFCodeFirst.Data.Seed
{
    public class SeedCategories
    {
        public Categories[] Categories =>
            new Categories[]
            {
                new Categories(){ CategoryId = 1, CategoryName = "Beverages", Description = "Soft drinks, coffees, teas, beers, and ales" },
                new Categories(){ CategoryId = 2, CategoryName = "Condiments", Description = "Sweet and savory sauces, relishes, spreads, and seasonings" },
                new Categories(){ CategoryId = 3, CategoryName = "Confections", Description = "Desserts, candies, and sweet breads" },
                new Categories(){ CategoryId = 4, CategoryName = "Dairy Products", Description = "Cheeses" },
                new Categories(){ CategoryId = 5, CategoryName = "Grains/Cereals", Description = "Breads, crackers, pasta, and cereal" },
                new Categories(){ CategoryId = 6, CategoryName = "Meat/Poultry", Description = "Prepared meats" },
                new Categories(){ CategoryId = 7, CategoryName = "Produce", Description = "Dried fruit and bean curd" },
                new Categories(){ CategoryId = 8, CategoryName = "Seafood", Description = "Seaweed and fish" }
            };
    }
}