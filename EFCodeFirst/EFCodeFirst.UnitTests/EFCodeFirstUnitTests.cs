using EFCodeFirst.Data;
using EFCodeFirst.UnitTests.Data;
using EFCodeFirst.ViewModels;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EFCodeFirst.UnitTests
{
    // ������� 1.
    // ������������ ������ ��� Northwind ��������� ������ EF Code First.
    // ��� �������� ����������������� ��������� ������ �������: 
    // � ������� ������ �������, �� ����� ���������(�.�.�� ������, � ������� �������� �������� ������������ ���������)
    // � ������� ������ ��������
    // o ������ ��������� �����
    // o ��� ���������
    // o ����� ���������

    public class EFCodeFirstUnitTests
    {

        [Fact]
        public void Check_Creation_Database_Using_Code_First_Approach_Success()
        {
            using (var context = new EFCodeFirstContext())
            {
                // Arrange
                const int categoryId = 2;
                // Data retrieved from db for CategoryId equals 2.
                var expectedOrdersByCategory = new RetrivedFromDbOrdersByCategory().OrdersByCategory;

                // Act
                var orderDetailsOfAssingProductCategory = context.OrderDetails
                    .Include(od => od.Order)
                        .ThenInclude(o => o.Customer)
                    .Include(od => od.Product)
                        .ThenInclude(p => p.Category)
                    .Where(c => c.Product.CategoryId == categoryId);

                var actualOrdersByCategory = new List<OrderByCategory>();

                foreach (var orderDetail in orderDetailsOfAssingProductCategory)
                {
                    var orderByCategory = new OrderByCategory()
                    {
                        CategoryName = orderDetail.Product.Category.CategoryName,
                        ProductId = orderDetail.ProductId,
                        ProductName = orderDetail.Product.ProductName,
                        OrderId = orderDetail.OrderId,
                        CompanyName = orderDetail.Order.Customer.CompanyName,
                        UnitPrice = orderDetail.UnitPrice,
                        Quantity = orderDetail.Quantity,
                        Discount = orderDetail.Discount
                    };
                    actualOrdersByCategory.Add(orderByCategory);
                }

                // Assert
                expectedOrdersByCategory.Should().BeEquivalentTo(actualOrdersByCategory);
            }
        }
    }
}
