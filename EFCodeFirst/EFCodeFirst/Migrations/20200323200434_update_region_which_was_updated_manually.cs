﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirst.Migrations
{
    public partial class update_region_which_was_updated_manually : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Region",
                keyColumn: "RegionId",
                keyValue: 2,
                column: "RegionDescription",
                value: "Western");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Region",
                keyColumn: "RegionId",
                keyValue: 2,
                column: "RegionDescription",
                value: "Eastern Europe");
        }
    }
}
