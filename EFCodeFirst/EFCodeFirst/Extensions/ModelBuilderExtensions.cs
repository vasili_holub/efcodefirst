﻿using EFCodeFirst.Data.Seed;
using EFCodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categories>().HasData(new SeedCategories().Categories);
            modelBuilder.Entity<Region>().HasData(new SeedRegions().Regions);
            modelBuilder.Entity<Territories>().HasData(new SeedTerritories().Territories);
            modelBuilder.Entity<Customers>().HasData(new SeedCustomers().Customers);
            modelBuilder.Entity<Employees>().HasData(new SeedEmployees().Employees);
            modelBuilder.Entity<EmployeeTerritories>().HasData(new SeedEmployeeTerritories().EmployeeTerritories);
            modelBuilder.Entity<Shippers>().HasData(new SeedShippers().Shippers);
            modelBuilder.Entity<Suppliers>().HasData(new SeedSuppliers().Suppliers);
            modelBuilder.Entity<Products>().HasData(new SeedProducts().Products);
            modelBuilder.Entity<Orders>().HasData(new SeedOrders().Orders);
            modelBuilder.Entity<OrderDetails>().HasData(new SeedOrderDetails().OrderDetails);
        }
    }
}
