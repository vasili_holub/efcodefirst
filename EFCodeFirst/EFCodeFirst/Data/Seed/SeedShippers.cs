﻿using EFCodeFirst.Models;

namespace EFCodeFirst.Data.Seed
{
    public class SeedShippers
    {
        public Shippers[] Shippers =>
            new Shippers[]
            {
                new Shippers(){ ShipperId = 1, CompanyName = "Speedy Express", Phone = "(503) 555-9831" },
                new Shippers(){ ShipperId = 2, CompanyName = "United Package", Phone = "(503) 555-3199" },
                new Shippers(){ ShipperId = 3, CompanyName = "Federal Shipping", Phone = "(503) 555-9931" }
            };
    }
}