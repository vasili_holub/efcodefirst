﻿using EFCodeFirst.Models;

namespace EFCodeFirst.Data.Seed
{
    public class SeedRegions
    {
        public Region[] Regions =>
            new Region[]
            {
                new Region() {RegionId = 1, RegionDescription = "Eastern"},
                new Region() {RegionId = 2, RegionDescription = "Western"},
                new Region() {RegionId = 3, RegionDescription = "Northern"},
                new Region() {RegionId = 4, RegionDescription = "Southern"},
                new Region() {RegionId = 5, RegionDescription = "Central Europe"}
            };
    }
}