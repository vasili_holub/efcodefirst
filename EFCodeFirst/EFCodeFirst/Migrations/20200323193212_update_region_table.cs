﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirst.Migrations
{
    public partial class update_region_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Region",
                keyColumn: "RegionId",
                keyValue: 2,
                column: "RegionDescription",
                value: "Eastern Europe");

            migrationBuilder.InsertData(
                table: "Region",
                columns: new[] { "RegionId", "RegionDescription" },
                values: new object[] { 5, "Central Europe" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Region",
                keyColumn: "RegionId",
                keyValue: 5);

            migrationBuilder.UpdateData(
                table: "Region",
                keyColumn: "RegionId",
                keyValue: 2,
                column: "RegionDescription",
                value: "Eastern");
        }
    }
}
